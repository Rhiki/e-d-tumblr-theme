# E:D Tumblr Theme
Theme for Tumblr blogs inspired by Elite: Dangerous' UI.

Feel free to use, modify, share or do whatever else you want to do with it, as long as you provide your changes freely and keep a link back here (a comment inside the HTML is just fine).


## Example [Live example here](https://cmdr-tonud.tumblr.com/)
![Example](https://bitbucket.org/Rhiki/e-d-tumblr-theme/raw/3b2d093d009b2bebf660f34c09d1159cb4159e2a/screenshots/Example_20180303.jpg)

## Applying the theme
Simply copy the contents of [this file](https://bitbucket.org/Rhiki/e-d-tumblr-theme/src/master/theme.html) into the Edit HTML of Tumblr's theme customization.

My use cases are pretty much only posting pictures and videos, so those are the only things I tested this with.

If you encounter any problems, have any questions or need help with anything, feel free to [message me on tumblr](https://cmdr-tonud.tumblr.com/), or [open an issue on BitBucket](https://bitbucket.org/Rhiki/e-d-tumblr-theme/issues?status=new&status=open).

## Customization options:
 * Add a background image
 * Show/Hide title (or custom title if set) in sidebar
 * Overwrite the blog title with a custom title
 * Show/Hide blog description
 * Show CMDR name; Hidden if no name is put into the CMDR field
 * Show up to 3 wingmen; Enable the "Show wingmen" option and insert names into "Wingman 1,2,3"
 * Show/Hide archive link
 * Show/Hide ask and submit link depending on your tumblr setting
 * Supports Custom CSS in case you want to change anything, without tinkering with the theme itself.
![Customization](https://bitbucket.org/Rhiki/e-d-tumblr-theme/raw/3b2d093d009b2bebf660f34c09d1159cb4159e2a/screenshots/Customization_20180303.png)
